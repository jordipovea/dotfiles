# Prompt customization
case "$TERM" in
    "dumb") # for emacs tramp
	PS1=">"
	;;
    xterm*|rxvt*|screen*)
	PS1="\[\033[1;32m\]\u@\h\[\033[1;0m\]:\[\033[1;34m\]\w\[\033[1;0m\]\n\$\[\033[0m\] "
	;;
    *)
	PS1=">"
	;;
esac

# Set umask
umask 022

# Environment variables
export PATH=$PATH:$HOME/bin
export LC_MESSAGES='en_US.UTF-8'
export LANG='en_US.UTF-8'
export BLOCKSIZE=1M
export PAGER='less'
export LESS='-imR -x 2'
export VISUAL=emacs
export EDITOR=emacs
export NO_COLOR=1
export COLORTERM=truecolor
export PYTHONSTARTUP="$HOME/python-startup.py"
#export PKG_PATH=https://mirror.planetunix.net/%m
export DOCKER_HOST=ssh://100.65.0.2
export MOZ_ACCELERATED=1
# For dark mode in qt apps, see https://unix.stackexchange.com/questions/745499/how-to-enable-dark-theme-for-qt-applications
export QT_QPA_PLATFORMTHEME=qt5ct
export XDG_RUNTIME_DIR=/tmp/runtime-jjpg

# Aliases
alias ls='ls -Fkh'
alias la='ls -a'
alias ll='ls -l'
alias lla='ls -la'
alias mv='mv -vi'
alias cp='cp -vi'
alias rm='rm -i'
alias transd='transmission-daemon'
alias transr='transmission-remote'
alias dotfiles='git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
alias hdmiontv='xrandr --output DP-3 --auto --left-of eDP-1'
alias hdmionmonitor='xrandr --output eDP-1 --auto --output DP-3 --auto --scale 1.3x1.3 --left-of eDP-1'
alias hdmioff='xrandr --output DP-3 --off'
# for cups
alias lpr='/usr/local/bin/lpr'
alias lpq='/usr/local/bin/lpq'
alias lprm='/usr/local/bin/lprm'
