" No vi compatibility
set nocompatible

" Allow backspacing over everything in insert mode.
set backspace=indent,eol,start

" Use utf-8
set encoding=utf-8

" Always show status line
set laststatus=2

" Show the cursor position all the time
set ruler
" Display incomplete commands
set showcmd
" Display completion matches in a status line
set wildmenu

" Keep 200 lines of command lie history
set history=200

" Show a few line of context around the cursor
set scrolloff=2

" Colors
set termguicolors
colorscheme selenized_bw
set background=light

" Activate line numbers
set number
" Activate relative line numbers
set relativenumber

" Higlight searches
set hlsearch
" Activate incremental searchin
set incsearch
" Ignore case when searching
set ignorecase
" Override ignorecase if search contains upper case
set smartcase

" Activate undo files
if has("persistent_undo")
    " Keep undo files in their own directory
    if !isdirectory($HOME."/.vim/undo")
	call mkdir($HOME."/.vim/undo", "p", 0700)
    endif
    set undodir=~/.vim/undo
    set undofile
endif

" When editing a file, always jump to the last known cursor position.
" Don't do it when the position is invalid, when inside an event handler
" (happens when dropping a file on gvim) and for a commit message (it's
" likely a different one than last time).
autocmd BufReadPost *
  \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
  \ |   exe "normal! g`\""
  \ | endif

" Enable file type detection
filetype plugin on
" Load indent files, to automatically do language-dependent indenting
filetype indent on
" Activate syntax highlighting
syntax on

" Change shift and tabs width
set shiftwidth=4
set tabstop=4
" Activate smart tab
set smarttab
" Activate autoindent
set autoindent
" Activate smartindent
set smartindent
" Show matching brackets
set showmatch

" Maximun width of text
set textwidth=79
" Show width column
set colorcolumn=80

" :W doas save the file, for system files
command! W execute 'w !doas tee % > /dev/null'
