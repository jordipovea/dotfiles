;; custom set variables -*- lexical-binding: t; -*-

(defvar bootstrap-version)
(let ((bootstrap-file
    (expand-file-name
      "straight/repos/straight.el/bootstrap.el"
      (or (bound-and-true-p straight-base-dir)
        user-emacs-directory)))
    (bootstrap-version 7))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
      (url-retrieve-synchronously
       "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
       'silent 'inhibit-cookies)
    (goto-char (point-max))
    (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)
(setq straight-use-package-by-default t)
(setq use-package-always-ensure t)

;; UTF-8 everywhere
(use-package emacs
  :init
  (set-charset-priority 'unicode)
  (setq locale-coding-system 'utf-8
        coding-system-for-read 'utf-8
        coding-system-for-write 'utf-8)
  (set-terminal-coding-system 'utf-8)
  (set-keyboard-coding-system 'utf-8)
  (set-selection-coding-system 'utf-8)
  (prefer-coding-system 'utf-8)
  (setq default-process-coding-system '(utf-8-unix . utf-8-unix)))

(use-package org-contrib
  :config
  (require 'ox-md)
  (require 'ox-confluence))

;; Set font
(set-face-attribute 'default nil :font "Iosevka" :height 110)

;; Disable the menu
(menu-bar-mode -1)
;; Disable the tool bar
(tool-bar-mode -1)
;; Disable the scroll bar
(scroll-bar-mode -1)
;; Disable the initial welcome screen
(setq inhibit-splash-screen t)
;; Ask for textual confirmation istead of GUI
(setq use-file-dialog t)

;; Disable ido mode
(ido-mode -1)

;; Save files~ and #files# in their own directories
(make-directory "~/.emacs.d/backups/" t)
(make-directory "~/.emacs.d/autosave/" t)
(setq auto-save-file-name-transforms '((".*" "~/.emacs.d/autosave/" t)))
(setq backup-directory-alist '(("." . "~/.emacs.d/backups/")))

;; Make Emacs repeat the C-u C-SPC command (`set-mark-command') by
;; following it up with another C-SPC.  It is faster to type
;; C-u C-SPC, C-SPC, C-SPC, than C-u C-SPC, C-u C-SPC, C-u C-SPC...
(setq set-mark-command-repeat-pop t)

;; By default, the built-in `savehist-mode' only keeps a record of
;; minibuffer histories.  This is helpful as it surfaces the most
;; recently selected items to the top, allowing you to access them again
;; very quickly.  With the variable `savehist-additional-variables' we
;; can make `savehist-mode' keep a record of any variable we want, so
;; that it persists between Emacs sessions.  I do this to store the
;; `kill-ring' and the `register-alist'.
(setq savehist-additional-variables '(register-alist kill-ring))

(savehist-mode 1)

;; Save bookmarks as soon as a bookmark is deleted or added
(setq bookmark-save-flag 1)

;; Sentences do not have two spaces between them
(setq sentence-end-double-space nil)

;; Require files ending in newline
(setq require-final-newline t)

;; Inhibit implied resize
(setq frame-inhibit-implied-resize t)

;; Kill whole line if at column zero
(setq kill-whole-line t)

;; Rebind C-x C-b, which is to list buffers, to ibuffer
(global-set-key [remap list-buffers] 'ibuffer)
;; Bind M-o to switch widnow
(global-set-key (kbd "M-o") 'other-window)

;; Use hippie expand instead of DAbbrev
(global-set-key [remap dabbrev-expand] 'hippie-expand)

;; Remember and restore the last cursor location of opened files
(save-place-mode 1)

;; Display column number in the modeline
(column-number-mode t)

;; Fill configuration
;; Set fill column to 79
(setq-default fill-column 79)
;; Display fill column indicator
(global-display-fill-column-indicator-mode t)

;; Display line numbers
(global-display-line-numbers-mode t)

;; leave 4 linex of context when screen moving
(setq next-screen-context-line 4)

;; dired configuration
;; When there are two Dired buffers side-by-side make Emacs automatically
;; suggest the other one as the target of copy or rename operations
(setq dired-dwim-target t)
;; Automatically hide the detailed listing when visiting a Dired buffer
(add-hook 'dired-mode-hook #'dired-hide-details-mode)

;; isearch configuration
;; Display a counter showing the number of the current and the other matches.
;; Place it before the prompt, though it can be after it.
(setq isearch-lazy-count t)
(setq lazy-count-prefix-format "(%s/%s) ")
(setq lazy-count-suffix-format nil)
;; Make regular Isearch interpret the empty space as a regular expression that
;; matches any character between the words you give it.
(setq search-whitespace-regexp ".*?")

;; Install wgrep package
(use-package wgrep)

;; Install and configure modus-themes
(use-package modus-themes
  :demand
  :bind (("<f5>" . modus-themes-toggle))
  :config
  ;; Disable other themes when loading modus themes
  (setq modus-themes-disable-other-themes t)
  ;; Enable italics fonts
  (setq modus-themes-italic-constructs t)
  ;; Enable bold fonts
  (setq modus-themes-bold-constructs t)

  (setq modus-themes-completions
	'((matches . (extrabold underline))
          (selection . (semibold italic))))

  ;; Maybe define some palette overrides, such as by using our presets
  (setq modus-themes-common-palette-overrides
        modus-themes-preset-overrides-intense)

  ;; Load modus-vivendi theme
  (modus-themes-load-theme 'modus-vivendi))

;; Install whole-line-or-region package
(use-package whole-line-or-region
  :config
  (whole-line-or-region-global-mode 1))

(use-package vertico
  :config
  (setq vertico-cycle t)
  (setq vertico-resize nil)
  (vertico-mode 1))

(use-package marginalia
  :config
  (marginalia-mode 1))

(use-package orderless
  :config
  (setq completion-styles '(orderless basic)))

(use-package consult
  :ensure t
  :bind (;; A recursive grep
         ("M-s M-g" . consult-grep)
         ;; Search for files names recursively
         ("M-s M-f" . consult-find)
         ;; Search through the outline (headings) of the file
         ("M-s M-o" . consult-outline)
         ;; Search the current buffer
         ("M-s M-l" . consult-line)
         ;; Switch to another buffer, or bookmarked file, or recently
         ;; opened file.
         ("M-s M-b" . consult-buffer)))

(use-package embark
  :ensure t
  :bind (("C-." . embark-act)
         :map minibuffer-local-map
         ("C-c C-c" . embark-collect)
         ("C-c C-e" . embark-export)))

;; The `embark-consult' package is glue code to tie together `embark'
;; and `consult'.
(use-package embark-consult
  :ensure t)

;; The built-in `savehist-mode' saves minibuffer histories.  Vertico
;; can then use that information to put recently selected options at
;; the top.
(savehist-mode 1)

;; The built-in `recentf-mode' keeps track of recently visited files.
;; You can then access those through the `consult-buffer' interface or
;; with `recentf-open'/`recentf-open-files'.
(recentf-mode 1)

(use-package yasnippet
  :config
  (use-package yasnippet-snippets)
  (yas-global-mode 1))

(use-package ispell
  :config
  ;; Configuring flyspell mode as per https://200ok.ch/posts/2020-08-22_setting_up_spell_checking_with_multiple_dictionaries.html

  ;; Configure LANG, otherwise ispell.el cannot find a 'default
  ;; dictionary' even though multiple dictionaries will be configured
  ;; in next line
  (setenv "LANG" "en_US.UTF-8")
  (setq ispell-program-name "hunspell")
  ;; Configure dictionaries
  (setq ispell-dictionary "en_US,es_MX")
  ;; ispell-set-spellchecker-params has to be called
  ;; before ispell-hunspell-add-multi-dic will work
  (ispell-set-spellchecker-params)
  (ispell-hunspell-add-multi-dic "en_US,es_MX")
  ;; For saving words to the personal dictionary, don't infer it from
  ;; the locale, otherwise it would save to ~/.hunspell_de_DE
  (setq ispell-personal-dictionary "~/.hunspell-personal"))

(use-package yaml-mode
  :mode "\\.yml\\'" "\\.yaml\\'"
  )
  
;; (use-package treesit-auto
;;   :custom
;;   (treesit-auto-install 'prompt)
;;   :config
;;   (treesit-auto-add-to-auto-mode-alist 'all)
;;   (global-treesit-auto-mode))

(use-package magit)

;; Configure python-mode
(use-package python
  :mode ("\\.py\\'" . python-mode)
  :interpreter ("ipython" . python-mode)
  :init
  (setq python-shell-interpreter "ipython")
  (setq python-shell-interpreter-args "--simple-prompt"))

;; Install and configure elfeed
(use-package elfeed
  :bind ("C-x w" . elfeed)
  :config
  (setq elfeed-feeds
	'("https://unixdigest.com/feed.rss"
	  "https://jcs.org/rss"
	  "https://mwl.io/feed"
	  "https://protesilaos.com/codelog.xml"
	  "https://protesilaos.com/commentary.xml"
	  "https://www.youtube.com/feeds/videos.xml?channel_id=UCw7Bz6EHxlnOoBUBlJZCWCw"
	  "https://research.exoticsilicon.com/feed.atom"
	  "https://utcc.utoronto.ca/~cks/space/blog/?atom")))

 ;; '(default ((t (:family "Iosevka" :foundry "UKWN" :slant normal :weight regular :height 110 :width normal)))))

(use-package pdf-tools
  :init
  ;; Open PDF files with pdf-view-mode
  (add-to-list 'auto-mode-alist '("\\.pdf\\'" . pdf-view-mode)))
