;; custom set variables -*- lexical-binding: t; -*-

;; Disable GUI elements early
(push '(menu-bar-lines . 0) default-frame-alist)
(push '(tool-bar-lines . 0) default-frame-alist)
(push '(vertical-scroll-bars) default-frame-alist)

;; Prevent package.el from loading
(setq package-enable-at-startup nil)
